/* Given an unknown prime p ranging from 2 to 10^9, we need to guess it based on x^2%p and y^2%p. We are free to select x, get the input and then decide on y based on that.*/

#include <bits/stdc++.h>
typedef unsigned int num;
typedef std::set<unsigned int> int_set;
#define p 179424673 /*change this to test another prime*/

int_set primeFactors(num n)
{
    int_set set_of_prime_factors;
    // Print the number of 2s that divide n
    while (n % 2 == 0) {
        set_of_prime_factors.insert(2);
        n = n / 2;
    }

    // n must be odd at this point.  So we can skip
    // one element (Note i = i +2)
    for (int i = 3; i <= sqrt(n); i = i + 2) {
        // While i divides n, print i and divide n
        while (n % i == 0) {

            set_of_prime_factors.insert(i);
            n = n / i;
        }
    }

    // This condition is to handle the case when n
    // is a prime number greater than 2
    if (n > 2)
        set_of_prime_factors.insert(n);

    return set_of_prime_factors;
}

int main()
{
    std::ios_base::sync_with_stdio(false);

    /*
    1. Step 1 -> Given a large x, lets find x^2 modulo p
    */
    num x = 32768;
    num first_dividend = x * x;
    num first_modulo = first_dividend % p;
    num first_multiple = first_dividend - first_modulo;

    /*
    2. In an ideal case, the "first_multiple " is our prime number.
    In any case, lets prime factorize it.
    */
    int_set list_of_primefactors = primeFactors(first_multiple);

    /*
    3. Lets check size of this set. If it has more than one element
    Lets process the set.
    */

    num number_of_factors = list_of_primefactors.size();
    num largest_factor = *(list_of_primefactors.rbegin());

    if (number_of_factors == 1) {
        std::cout << "The actual value of p was " << p << " The guessed value was " << largest_factor << "\n";
        return 0;
    }
    else {
        int_set unique_modulo_of_squares;
        int_set::iterator it;
        x = std::ceil(std::sqrt(largest_factor));
        while (unique_modulo_of_squares.size() != number_of_factors) {
            unique_modulo_of_squares.clear();

            for (it = list_of_primefactors.begin(); it != list_of_primefactors.end(); it++) {

                unique_modulo_of_squares.insert((x * x) % (*it));
            }
            if (unique_modulo_of_squares.size() == number_of_factors)
                break;
            x++;
        }
        /* now we have x which can uniquely resolve the prime */
        num second_dividend = x * x;
        num second_modulo = second_dividend % p;
        num second_multiple = second_dividend - second_modulo;
        num p_final = std::__gcd(first_multiple, second_multiple);

        std::cout << "The actual value of p was " << p << " The guessed value was " << p_final << "\n";
    }
}

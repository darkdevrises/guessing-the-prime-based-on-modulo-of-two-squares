Shef chose some prime number P between 2 and 10^9 inclusive. You want to guess it.

Before that, you may ask Shef at most M questions, since he is quite busy. In each question, you tell Shef an integer x (1 ≤ x ≤ 10^9) and Shef tells you x^2 modulo P. At the end, you guess Shef's prime P and he tells you if your guess was correct.

However, Shef will sometimes cheat: he can change his prime any number of times during the game (even after you tell him your guess), but only in such a way that all his answers to your previous questions remain correct.

Show Shef that you can always guess the correct prime, even if he tries to cheat! Do it in two tries!